package com.stefanstegic.CryptocurrencyRecommender;

import com.stefanstegic.CryptocurrencyRecommender.drools.AssemblyAndFilterTests;
import com.stefanstegic.CryptocurrencyRecommender.drools.BadInputFormatTests;
import com.stefanstegic.CryptocurrencyRecommender.drools.RecommendationTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AssemblyAndFilterTests.class,
        BadInputFormatTests.class,
        RecommendationTests.class
})

public class TestAll {

}
