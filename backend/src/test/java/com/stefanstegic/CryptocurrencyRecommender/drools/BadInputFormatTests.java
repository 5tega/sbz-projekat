package com.stefanstegic.CryptocurrencyRecommender.drools;

import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import com.stega.cryptorecommender.util.CryptoJsonUtil;
import com.stega.cryptorecommender.util.KieUtil;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.junit.Assert;

import java.io.FileNotFoundException;
import java.util.Collection;

public class BadInputFormatTests {

    @Test
    public void badMetadataFormat() {
        KieSession kieSession = KieUtil.newTestingSession();
        MetadataDTO metadataDTO = new MetadataDTO();

        FactHandle handle = kieSession.insert(metadataDTO);
        kieSession.fireAllRules();
        MetadataDTO result = (MetadataDTO) kieSession.getObject(handle);

        Assert.assertNull(result);

        kieSession.dispose();
    }

    @Test
    public void badTickerFormat() {
        KieSession kieSession = KieUtil.newTestingSession();
        TickerDTO tickerDTO = new TickerDTO();

        FactHandle handle = kieSession.insert(tickerDTO);
        kieSession.fireAllRules();
        TickerDTO result = (TickerDTO) kieSession.getObject(handle);

        Assert.assertNull(result);

        kieSession.dispose();
    }

    @Test
    public void goodMetadataFormat() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();
        MetadataDTO metadataDTO = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");

        FactHandle handle = kieSession.insert(metadataDTO);
        kieSession.fireAllRules();
        MetadataDTO result = (MetadataDTO) kieSession.getObject(handle);

        Assert.assertNotNull(result);

        kieSession.dispose();
    }

    @Test
    public void goodTickerFormat() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();
        TickerDTO tickerDTO = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");

        FactHandle handle = kieSession.insert(tickerDTO);
        kieSession.fireAllRules();
        TickerDTO result = (TickerDTO) kieSession.getObject(handle);

        Assert.assertNotNull(result);

        kieSession.dispose();
    }

}
