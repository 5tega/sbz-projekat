package com.stega.cryptorecommender;

import com.stega.cryptorecommender.dto.CryptoCurrencyRecommendationDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

@SpringBootApplication
public class CryptocurrencyRecommenderApplication {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = SpringApplication.run(CryptocurrencyRecommenderApplication.class, args);
	}
	
	@Bean
    public KieContainer kieContainer() {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks
                .newKieContainer(ks.newReleaseId("com.stefanstegic", "SBZDrools", "1.0.0-SNAPSHOT"));
        KieScanner kScanner = ks.newKieScanner(kContainer);
        kScanner.start(10_000);
        return kContainer;
    }

}
