package com.stega.cryptorecommender.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import com.stega.cryptorecommender.util.CryptoJsonUtil;
import com.stega.cryptorecommender.util.MockDataHelper;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class NomicsCryptoService {
	private ArrayList<MetadataDTO> mockMetaCryptos;
	private ArrayList<TickerDTO> mockTickerCryptos;

	private RestTemplate restTemplate;
	private final Gson gson = new Gson();

	private MockDataHelper mockDataHelper;

	@Autowired
	public NomicsCryptoService(MockDataHelper mockDataHelper) throws IOException {
		this.mockDataHelper = mockDataHelper;

		restTemplate = new RestTemplate();
		String metaCryptosJson = mockDataHelper.loadCryptosMetadata("./src/main/resources/crypto-metadata-all.json");
		mockMetaCryptos = CryptoJsonUtil.metaCryptosFromJson(metaCryptosJson);

		String tickerCryptosJson = mockDataHelper.loadCryptosTicker("./src/main/resources/crypto-ticker-all.json");
		mockTickerCryptos = CryptoJsonUtil.tickerCryptosFromJson(tickerCryptosJson);

	}

	public ArrayList<MetadataDTO> getMockMetaCryptos() { return mockMetaCryptos;
	}

	public ArrayList<TickerDTO> getMockTickerCryptos() {
		return mockTickerCryptos;
	}

	public ArrayList<MetadataDTO> getMetaCryptos() {
		return getMockMetaCryptos();
//		ResponseEntity<String> response = restTemplate.getForEntity("https://api.nomics.com/v1/currencies?key=390addcb6f977d57f1396d935417bd0826ac10ee", String.class);
//
//		return metaCryptosFromJson(response.getBody());
	}

	public ArrayList<TickerDTO> getTickerCryptos() {
		return getMockTickerCryptos();
//		ResponseEntity<String> response = restTemplate.getForEntity("https://api.nomics.com/v1/currencies/ticker?key=390addcb6f977d57f1396d935417bd0826ac10ee", String.class);
//
//		return tickerCryptosFromJson(response.getBody());
	}
}
