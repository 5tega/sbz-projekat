package com.stega.cryptorecommender.services;

import com.stega.cryptorecommender.model.Cryptocurrency;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Component
public class PricePredictionService {

    private double predict(double currentPrice, double currentTime, double previousPrice, double previousTime, double predictTime) {
        double k = (currentPrice - previousPrice) / (currentTime - previousTime);
        double n = currentPrice - k * currentTime;

        return k * predictTime + n;
    }

    public double predictBasedOnWeek(Cryptocurrency c, int days) {
        double currentPrice = c.ticker.price;
        double currentTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        double previousPrice = c.ticker.price - c.ticker.monthly_delta.price_change;
        double previousTime = currentTime - 604800; // minus one week

        return predict(currentPrice, currentTime, previousPrice, previousTime, currentTime + days * 86400);
    }

    public double predictBasedOnMonth(Cryptocurrency c, int days) {
        double currentPrice = c.ticker.price;
        double currentTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        double previousPrice = c.ticker.price - c.ticker.monthly_delta.price_change;
        double previousTime = currentTime - 2592000; // minus one month

        return predict(currentPrice, currentTime, previousPrice, previousTime, currentTime + days * 86400);
    }

    public double predictBasedOnYear(Cryptocurrency c, int days) {
        double currentPrice = c.ticker.price;
        double currentTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        double previousPrice = c.ticker.price - c.ticker.yearly_delta.price_change;
        double previousTime = currentTime - 31536000; // minus one year

        return predict(currentPrice, currentTime, previousPrice, previousTime, currentTime + days * 86400);
    }

}
