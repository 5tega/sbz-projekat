package com.stega.cryptorecommender.filters;

import com.stega.cryptorecommender.model.Cryptocurrency;
import org.drools.core.ObjectFilter;
import org.springframework.stereotype.Component;

import com.stega.cryptorecommender.dto.CryptoCurrencyRecommendationDTO;


@Component
public class CryptocurrencyFilter implements ObjectFilter {
	@Override
    public boolean accept(Object o) {
        return o instanceof Cryptocurrency;
    }
}
