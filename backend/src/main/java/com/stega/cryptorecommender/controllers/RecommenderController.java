package com.stega.cryptorecommender.controllers;
import org.springframework.web.bind.annotation.*;

import com.stega.cryptorecommender.dto.*;
import com.stega.cryptorecommender.services.DroolsService;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(value = "/api/recommend", produces = MediaType.APPLICATION_JSON_VALUE)
public class RecommenderController {
	
	@Autowired
	private DroolsService droolsService;
	
	@PostMapping
	public ResponseEntity<RecommendationDTO> recommendCryptos(@RequestBody RecommendationParamsDTO params) {
		RecommendationDTO response = new RecommendationDTO();
		
		ArrayList<CryptoCurrencyRecommendationDTO> cryptos = droolsService.findSuggestedCryptos(params);
		response.setCryptocurrencies(cryptos);
		response.setDays(params.getDays());
		
		return new ResponseEntity<RecommendationDTO>(response, HttpStatus.OK);
	}
}
