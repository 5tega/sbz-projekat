package com.stega.cryptorecommender.util;

import com.stega.cryptorecommender.dto.CryptoCurrencyRecommendationDTO;
import com.stega.cryptorecommender.services.PricePredictionService;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.ArrayList;

public class KieUtil {
    public static KieServices kieServices = KieServices.Factory.get();
    public static KieContainer kieContainer = kieServices.newKieContainer(kieServices.newReleaseId("com.stefanstegic", "SBZDrools", "1.0.0-SNAPSHOT"));

    public static KieSession newTestingSession() {
        KieSession kSession = kieContainer.newKieSession();

        kSession.setGlobal("minMarketCapDollars", 0.0);
        kSession.setGlobal("maxMarketCapDollars", 10000000000000000.0);
        kSession.setGlobal("min24hVolume", 0.0);
        kSession.setGlobal("max24hVolume", 10000000000000000.0);
        kSession.setGlobal("intervalDays", 7);
        kSession.setGlobal("recommendations", new ArrayList<CryptoCurrencyRecommendationDTO>());
        kSession.setGlobal("pricePredictionService", new PricePredictionService());

        return kSession;
    }
}
