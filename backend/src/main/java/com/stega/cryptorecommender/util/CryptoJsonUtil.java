package com.stega.cryptorecommender.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class CryptoJsonUtil {
    private static Gson gson = new Gson();

    public static ArrayList<MetadataDTO> metaCryptosFromJson(String jsonString) {
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>(){}.getType();
        ArrayList<HashMap<String, String>> json_list = gson.fromJson(jsonString, type);

        ArrayList<MetadataDTO> result = new ArrayList<>();
        MetadataDTO metaCrypto;
        for (HashMap<String, String> json : json_list) {
            metaCrypto = new MetadataDTO();
            metaCrypto.original_symbol = json.get("original_symbol");
            json.remove("original_symbol");
            if (metaCrypto.original_symbol == null || metaCrypto.original_symbol.trim().isEmpty()) {
                continue;
            }

            metaCrypto.whitepaper_url = json.get("whitepaper_url");
            json.remove("whitepaper_url");

            metaCrypto.logo_url = json.get("logo_url");
            json.remove("logo_url");

            metaCrypto.website_url = json.get("website_url");
            json.remove("website_url");

            metaCrypto.name = json.get("name");
            json.remove("name");

            metaCrypto.used_for_pricing = Boolean.parseBoolean(json.get("used_for_pricing"));
            json.remove("used_for_pricing");

            metaCrypto.markets_count = Integer.parseInt(json.get("markets_count"));
            json.remove("markets_count");

            for (String key : json.keySet()) {
                if (key.endsWith("_url")) {
                    metaCrypto.socialMedia.put(key, json.get(key));
                }
            }

            result.add(metaCrypto);
        }

        return result;
    }

    public static ArrayList<TickerDTO> tickerCryptosFromJson(String jsonString) {
        Type type = new TypeToken<ArrayList<TickerDTO>>(){}.getType();
        ArrayList<TickerDTO> list = gson.fromJson(jsonString, type);
        ArrayList<TickerDTO> result = new ArrayList<>();
        for (TickerDTO t : list) {
            if (t.symbol == null || t.symbol.trim().isEmpty()) {
                continue;
            }
            result.add(t);
        }

        return result;
    }

    public static MetadataDTO metaCryptoFromJson(String jsonString) {
        return gson.fromJson(jsonString, MetadataDTO.class);
    }

    public static TickerDTO tickerCryptoFromJson(String jsonString) {
        return gson.fromJson(jsonString, TickerDTO.class);
    }

    public static MetadataDTO metaCryptoFromFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        return gson.fromJson(reader, MetadataDTO.class);
    }

    public static TickerDTO tickerCryptoFromFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        return gson.fromJson(reader, TickerDTO.class);
    }

}
