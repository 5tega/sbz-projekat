package com.stega.cryptorecommender.dto.nomics;

import lombok.Data;

@Data
public class CryptoDeltaInfo {
    public Double volume;
    public Double price_change;
    public Double volume_change;
    public Double market_cap_change;
}
