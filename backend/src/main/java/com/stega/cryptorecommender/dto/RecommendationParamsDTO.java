package com.stega.cryptorecommender.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
public class RecommendationParamsDTO {
	private int days;
	private double minMarketCapDollars, maxMarketCapDollars;
	private double min24hVolume, max24hVolume;
}
