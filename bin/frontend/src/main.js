import Vue from 'vue'
import App from './App.vue'
import routes from './router'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'

Vue.config.productionTip = false

Vue.use(Vuetify);
const vuetify = new Vuetify({
  theme: { dark: true }
});

Vue.use(VueRouter);
const router = new VueRouter({
  routes
});

new Vue({
  vuetify,
  router,
  render: h => h(App),
}).$mount('#app')
