# SBNZ predlog projekta "Cryptocurrency Reccomender"

Student: Stefan Stegić
Indeks: SW 61/2017

## Motivacija

Tržište kriptovaluta, kao najspekulativniji i najnemirniji koncept u ekonomskoj istoriji, deluje primamljivo amaterskom investitoru. Međutim, mnogi od njih nisu dovoljno ekonomski obrazovani i/ili nisu spremni da ulože vreme u istraživanje novčića, što bi moglo da im poboljša šanse za profit.

## Problem

Korisnik želi da investira u tržište kriptovaluta. Zna minimalni profit koji želi da ostvari, vremenski interval do prodaje i kriterijume za izbor novčića. Potreban mu je ekspertski sistem koji će uzeti podatke o trenutnom i istorijskom stanju na tržištu kriptovaluta i ponuditi obećavajuće novčiće za kupovinu.

## Metodologija

### Ulaz

1. Trenutno i istorijsko stanje na tržištu kriptovaluta (eksterni API)
2. Minimalni profit izražen u procentima
3. Vremenski interval
4. Min/max tržišna kapitalizacija novčića
5. Min/max promet u poslednjih 24h
6. Min/max broj jedinica u cirkulaciji

### Izlaz

Spisak obećavajućih kriptovaluta u skladu sa kriterijumima korisnika.

## Pravila

Najpre se filtriraju kriptovalute po korisničkim kriterijumima. Izbacuju se iz razmatranja novčići čije tržišne kapitalizacije, prometi u poslednjih 24h i brojevi jedinica u cirkulaciji ne upadaju u zadate opsege.

Zatim, nad istorijskim podacima dostupnih kriptovaluta se radi linearna regresija i prikupljaju se predikcije cena u trenutku isteka zadatog vremenskog intervala.

Najzad, korisniku se predstavljaju novčići koji mogu u tom vremenskom intervalu da ostvare zadati profit.

### Detaljniji spisak pravila

Kriptovaluta je aktivna i poklapa se sa zadatim kriterijumima (market cap, max supply, 24h volume)
=> Postaje kvalifikovana za obradu i inicijalizuje joj se ocena na 0 bodova

Kriptovaluta je kvalifikovana za obradu i ima barem jednu društvenu mrežu
=> Označava se kao prisutna na društvenim mrežama

Kriptovaluta je kvalifikovana za obradu i ima website
=> Označava se kao da ima website

Kriptovaluta je kvalifikovana za obradu i ima whitepaper
=> Označava se kao da ima whitepaper

Kriptovaluta ima website ILI whitepaper ILI je prusutna na barem jednoj jednoj društvenoj mreži
=> Nije scam coin

Kriptovaluta nije scam coin
=> Dobija bodove za svaku menjačnicu na kojoj se nalazi
=> Dobija bodove za svaki par kriptovaluta čiji je deo
=> Dobija bodove za svaku društvenu mrežu na kojoj se nalazi
=> Gubi bodove srazmerne rangu na osnovu tržišne kapitalizacije
=> Dobija bodove srazmerne broju dana od najstarije obavljene trgovine
=> Postaje preliminarno ocenjena

Kriptovaluta je preliminarno ocenjena i spada u top N
=> Postaje interesantna i inicijalizuje joj se konačna ocena na 0

Kriptovaluta je interesantna i ima pozitivan trend u poslednjih nedelju dana
=> Postaje kratkoročno rastuća i dobija bodove srazmerne rastu

Kriptovaluta je interesantna i ima pozitivan trend u poslednjih mesec dana
=> Postaje srednjoročno rastuća i dobija bodove srazmerne rastu

Kriptovaluta je interesantna i ima pozitivan trend u poslednjih godinu dana
=> Postaje dugoročno rastuća i dobija bodove srazmerne rastu

Kriptovaluta je srednjoročno ili dugoročno rastuća
=> Postaje rastuća

Kriptovaluta je srednjoročno rastuća i trenutna cena je najniža u poslednjih nedelju dana
=> Nalazi se u nedeljnom dipu

Kriptovaluta je dugoročno rastuća i trenutna cena je najniža u poslednjih mesec dana
=> Nalazi se u mesečnom dipu

Kriptovaluta je u nedeljnom ili mesečnom dipu
=> Kriptovaluta je u dipu i dobija bodove

Kriptovaluta je rastuća
=> Upisuje se predviđeni profit za zadati interval na osnovu regresione krive

Konačno, kriptovalute se uzimaju sa konačne rang liste dokle god ne zadovolje zadati profit.
